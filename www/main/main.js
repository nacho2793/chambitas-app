// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var mainModule = angular.module('main', ['ionic',  'lbServices', 'ngResource',  'ion-datetime-picker', 'ngStorage'])


.run(function($ionicPlatform, $ionicPickerI18n) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

  $ionicPickerI18n.weekdays = ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"];
  $ionicPickerI18n.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
  $ionicPickerI18n.ok = "Aceptar";
  $ionicPickerI18n.cancel = "Cancelar";
  $ionicPickerI18n.okClass = "button-positive";
  $ionicPickerI18n.cancelClass = "button-stable";
})

.config(function($stateProvider, $urlRouterProvider, LoopBackResourceProvider, $ionicConfigProvider) {
  LoopBackResourceProvider.setUrlBase('http://54.201.59.56:3000/api');
  //LoopBackResourceProvider.setUrlBase('http://aleate.ngrok.io/api');

  LoopBackResourceProvider.setAuthHeader('X-Access-Token');

  $urlRouterProvider.otherwise('/start/login');
  $ionicConfigProvider.tabs.position("bottom");
  $ionicConfigProvider.backButton.text('Atras');
  $ionicConfigProvider.navBar.alignTitle('center');
})

.controller('mainController', function($scope) {

})
