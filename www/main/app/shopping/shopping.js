!function() {
  mainModule.config(function($stateProvider){
    $stateProvider
    .state('app.shopping', {
      url:"/shopping",
      cache: false,
      views: {
        app: {
          templateUrl: 'main/app/shopping/shopping.html',
          controller: controller
        }
      },
      resolve: {}
    })
  });

  function controller($scope, $localStorage, $state, $ionicModal, $filter, $ionicScrollDelegate, $ionicPopup, $ionicLoading, Sale, Client/*, Notification*/){

    /*
    * Description:
    *   Search the item selected on the shopping Cart and show editShoppingModal.
    *
    * Parameters:
    *   index:    Int. Index from the item selected on the list of products
    */
    $scope.editProduct = function(index){
      $scope.productFound=$filter('filter')($scope.shopping, function(d){ return d.id==$scope.shopping[index].id; })[0];
      $scope.productFound.indexShopping = index;
      console.log($scope.productFound);
      $scope.editShoppingModal.show();
    };

    /*
    * Description:
    *   Close Modal continueShoppingModal and clean inputs.
    */
    $scope.closeConfirmOrder = function() {
      $scope.temporal = {};
      $scope.continueShoppingModal.hide();
    };

    /*
    * Description:
    *   Close Modal editShoppingModal, recalculates the total price of the product and Scroll the view to the top.
    *   Same operation as saveChange.
    */
    $scope.closeEditProduct = function() {
      $scope.productFound.totalPrice = getTotalPrice($scope.productFound.unitSelected, $scope.productFound.quantity);
      $ionicScrollDelegate.scrollTop(true);
      $scope.editShoppingModal.hide();
    };

    /*
    * Description:
    *   Close Modal editShoppingModal, recalculates the total price of the product and Scroll the view to the top.
    *   Same operation as closeEditProduct.
    */
    $scope.saveChange = function(){
      $scope.productFound.totalPrice = getTotalPrice($scope.productFound.unitSelected, $scope.productFound.quantity);
      $scope.closeEditProduct();
    };

    /*
    * Description:
    *   Initialize the time to order and show continueShoppingModal.
    */
    $scope.confirmOrder = function(){
      initiazTime();
      $scope.continueShoppingModal.show();
    };

    /*
    * Description:
    *   Pack the information and send it to the backend.
    */
    $scope.sendOrder = function(){
      var dateSale = new Date();
      var minDateSale = new Date();
      minDateSale.setHours(48);
      minDateSale.setMinutes(0);
      var desiredDate = moment([
        moment($scope.temporal.date_form).get('year'),
        moment($scope.temporal.date_form).get('month'),
        moment($scope.temporal.date_form).get('date'),
        moment($scope.temporal.time_form).get('hour'),
        moment($scope.temporal.time_form).get('minute')
      ]);

      if(desiredDate > minDateSale){
        console.log(desiredDate);
        console.log(minDateSale);
        $ionicLoading.show();
        var order = new Sale({'status': 'En espera', 'products': $scope.shopping, 'clientId': Client.getCurrentId(), 'comments': $scope.temporal.comments, 'totalPrice': $scope.getTotal(), 'desiredDate': desiredDate, 'dateSale': dateSale});
        console.log(order);
        order.$save(function succes(data){
          /*
          En espera de Backend
          Notification.send({method: 'email', type: 'saleRequest', userId:'', extraInfo:{} });
          Notification.send({method: 'email', type: 'saleRequest', userId:'', extraInfo:{} });
          */
          $ionicLoading.hide();
          console.log(data);
          var alertPopup = $ionicPopup.alert({
            title: 'Enviado',
            template: 'El pedido ha sido enviado.'
          });

          alertPopup.then(function(res) {
            if(res){
              $scope.shopping.splice(0, $scope.shopping.length)
              $ionicScrollDelegate.scrollTop(true);
              $scope.closeConfirmOrder();
            }
          });
        },
        function error(data){
          $ionicLoading.hide();
          console.log(data);
        });
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'Alerta',
          template: 'La Fecha de entrega debe ser minimo dentro de dos dias.'
        });

        alertPopup.then();
      }

    };

    /*
    * Description:
    *   Calculates the totalPrice to the item selected on process. This function is run by sendOrder.
    */
    $scope.getTotal = function getTotal(){
      var total = 0;
      for(var i = 0; i < $scope.shopping.length; i++){
        var product = $scope.shopping[i];
        total += product.totalPrice;
      }
      return total;
    };

    /*
    * Description:
    *   Removes the item selected from the Shopping Cart.
    */
    $scope.deleteProduct = function(){
      var alertPopup = $ionicPopup.confirm({
        title: 'Alerta',
        template: '¿Estas seguro de que deseas eliminar este producto?'
      });

      alertPopup.then(function(res) {
        if(res){
          $scope.shopping.splice($scope.productFound.indexShopping, 1);
          $scope.closeEditProduct();
        }
      });
    };

    /*
    * Description:
    *   Removes all items from the Shopping Cart.
    */
    $scope.emptyShopping = function(){
      var alertPopup = $ionicPopup.confirm({
        title: 'Alerta',
        template: '¿Estas seguro de que deseas vaciar el carrito?'
      });

      alertPopup.then(function(res) {
        if(res){
          $scope.shopping.splice(0, $scope.shopping.length);
          $ionicScrollDelegate.scrollTop(true);
        }
      });
    };

    /*
    * Description:
    *   Decreases the quantity from the item selected.
    */
    $scope.minusProd = function(){
      $scope.productFound.quantity--;
    };

    /*
    * Description:
    *   Increases the quantity from the item selected.
    */
    $scope.plusProd = function(){
      $scope.productFound.quantity++;
    };

    /*
    * Description:
    *   Calculates the totalPrice to the item selected. This function is run by closeEditProduct and saveChange.
    *
    * Parameters:
    *   typeUnit:   String. Indicates the type of unit selected
    *   quantity:   Int. The quantity of products was selected.
    */
    function getTotalPrice(typeUnit, quantity){
      var unitSelected = $filter('filter')($scope.productFound.units, function(d){ return d.unit==typeUnit; })[0];
      return quantity*unitSelected.price;
    };

    /*
    * Description:
    *   Initialize the time_form to handle accurate minute intervals.
    */
    function initiazTime(){
      if(!$scope.temporal.time_form){
        $scope.temporal.time_form = new Date();
        switch (true) {
          case moment().get('minute') > 0 && moment().get('minute') <= 7:
          $scope.temporal.time_form.setMinutes(0);
          break;
          case moment().get('minute') > 7 && moment().get('minute') <= 22:
          $scope.temporal.time_form.setMinutes(15);
          break;
          case moment().get('minute') > 22 && moment().get('minute') <= 37:
          $scope.temporal.time_form.setMinutes(30);
          break;
          case moment().get('minute') > 37 && moment().get('minute') <= 52:
          $scope.temporal.time_form.setMinutes(45);
          break;
          case moment().get('minute') > 52 && moment().get('minute') <= 59:
          $scope.temporal.time_form.setMinutes(0);
          break;
          default:
          $scope.temporal.time_form.setMinutes(0);
        }
      }

      if(!$scope.temporal.date_form){
        $scope.temporal.date_form = new Date();
        $scope.temporal.date_form .setHours(48)
      }
    };

    /*
    * Description:
    *   Initialize basic variables and scopes from the view.
    */
    function variables(){
      $scope.temporal = {};
      $scope.shopping = $localStorage.shopping;
      console.log($scope.shopping);

      $ionicModal.fromTemplateUrl('main/app/shopping/continueShoppingModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.continueShoppingModal = modal;
      });

      $ionicModal.fromTemplateUrl('main/app/shopping/editShoppingModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.editShoppingModal = modal;
      });
    };

    function init(){
      variables();
    };

    init();
  }
}();
