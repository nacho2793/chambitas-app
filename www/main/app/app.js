! function() {
  mainModule.config(function($stateProvider) {
    $stateProvider
      .state('app', {
        url: "/app",
        views: {
          main: {
            templateUrl: 'main/app/app.html',
            controller: controller
          }
        },
        resolve: {}
      })
  });

  function controller($scope, $localStorage, $state, $ionicPopup, $ionicHistory, AppUser) {

    $scope.logout = function() {
      var alertPopup = $ionicPopup.confirm({
        title: 'Alerta',
        template: '¿Estas seguro de que deseas salir de Fruteria?'
      });

      alertPopup.then(function(res) {
        if (res) {
          AppUser.logout(
            function success(data) {
              console.log('logout - success', data);
              $state.go('start.login');
              $localStorage.$reset();
              $ionicHistory.nextViewOptions({
                historyRoot: true
              });
            },
            function error(data) {
              console.log('logout - failure ', data);
            });
        }
      });
    };

    /*
     * Description:
     *   Check the quantity of products in the shopping cart and show the badge on the Shopping tab.
     */
    $scope.getShoppingBadge = function() {
      if (!$localStorage.shopping) {
        $localStorage.shopping = [];
      }
      return $localStorage.shopping.length;
    }
  }
}();
