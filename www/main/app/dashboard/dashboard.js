!function() {
  mainModule.config(function($stateProvider){
    $stateProvider
    .state('app.dashboard', {
      url:"/dashboard",
      views: {
        app: {
          templateUrl: 'main/app/dashboard/dashboard.html',
          controller: controller
        }
      },
      resolve: {}
    })
  });

  function controller($scope){
  }
}();
