!function() {
  mainModule.config(function($stateProvider){
    $stateProvider
    .state('app.dashboard.products', {
      url:"/products",
      views: {
        dashboard: {
          templateUrl: 'main/app/dashboard/products/products.html',
          controller: controller
        }
      },
      resolve: {}
    })
  });

  function controller($rootScope, $scope, $localStorage, $state, $ionicModal, $ionicLoading, $filter, $ionicScrollDelegate, Producto, Category, $ionicPopup, Client){

    /*
    * Description:
    *   Close native keyboard when 'Enter' is pressed.
    */
    $scope.pressSearch = function(e){
      if(e.key == 'Enter'){
        cordova.plugins.Keyboard.close();
      }
    };

    /*
    * Description:
    *   Remove the name on the searchProducts Scope and update the list of results.
    */
    $scope.removeSearch = function(){
      $scope.searchProducts.name = "";
      $scope.changeSearch();
    };

    /*
    * Description:
    *   Search the item selected on the shopping Cart and show productModal.
    *
    * Parameters:
    *   index:    Int. Index from the item selected on the list of products
    *   side:     String. Indicates which side of the list item was selected. The list of products it is separated on two list, "right" and "left"
    */
    $scope.selectItem = function(id){
      $scope.productFound=$filter('filter')($localStorage.shopping, function(d){return d.id==id; })[0];

      if(!$scope.productFound){
        $scope.productFound=$filter('filter')($scope.products, function(d){ return d.id==id; })[0];
        $scope.productFound.quantity = 0;
        $scope.productFound.unitSelected = "";
        $scope.productModal.show();
      } else {
        $scope.productFound.indexShopping = $localStorage.shopping.indexOf($scope.productFound);
        $scope.editShoppingModal.show();
      }
      console.log($scope.productFound);

    };

    /*
    * Description:
    *   Close Modal editProduct, recalculates the total price of the product and Scroll the view to the top.
    *   Same operation as closeEditProduct.
    */
    $scope.saveChange = function(){
      $scope.productFound.totalPrice = getTotalPrice($scope.productFound.unitSelected, $scope.productFound.quantity);
      $scope.closeEditProduct();
    };

    /*
    * Description:
    *   Removes the item selected from the Shopping Cart.
    */
    $scope.deleteProduct = function(){
      var alertPopup = $ionicPopup.confirm({
        title: 'Alerta',
        template: '¿Estas seguro de que deseas eliminar este producto?'
      });

      alertPopup.then(function(res) {
        if(res){
          $localStorage.shopping.splice($scope.productFound.indexShopping, 1);
          $scope.closeEditProduct();
        }
      });
    };

    /*
    * Description:
    *   Close Modal productModal and Scroll the view to the top.
    */
    $scope.closeProductModal = function() {
      $ionicScrollDelegate.scrollTop(true);
      $scope.productModal.hide();
    };

    /*
    * Description:
    *   Close Modal editProduct, recalculates the total price of the product and Scroll the view to the top.
    *   Same operation as saveChange.
    */
    $scope.closeEditProduct = function() {
      $scope.productFound.totalPrice = getTotalPrice($scope.productFound.unitSelected, $scope.productFound.quantity);
      $ionicScrollDelegate.scrollTop(true);
      $scope.editShoppingModal.hide();
    };

    /*
    * Description:
    *   Goes to the Categories view.
    */
    $scope.gotoCategories = function(){
      $scope.searchProducts = $localStorage.searchProducts = {};
      $state.go('app.dashboard.categories');
    };

    /*
    * Description:
    *   Decreases the quantity from the item selected.
    */
    $scope.minusProd = function(){
      $scope.productFound.quantity--;
    };

    /*
    * Description:
    *   Increases the quantity from the item selected.
    */
    $scope.plusProd = function(){
      $scope.productFound.quantity++;
    };

    /*
    * Description:
    *   Calculates the totalPrice using the function getTotalPrice, add the product selected to the Shopping Cart and Close the productModal.
    */
    $scope.addToShopping = function(){
      $scope.productFound.totalPrice = getTotalPrice($scope.productFound.unitSelected, $scope.productFound.quantity);
      $localStorage.shopping.push($scope.productFound);
      console.log($localStorage.shopping);
      $scope.closeProductModal();
      //$ionicLoading.show({ template: 'Agregado a carrito', noBackdrop: true/*, duration: 2000*/ });
    };

    /*
    * Description:
    *   Check if the processed item is in the shopping cart. This scope is run each time the product view is displayed.
    *
    * Parameters:
    *   index:    Int. Index from the item processed on the list of products
    *   side:     String. Indicates which side of the list item was selected. The list of products it is separated on two list, "right" and "left"
    */
    $scope.onShopping = function(index, side){
      if($localStorage.shopping.length > 0){
        if(side == 'left'){
          var productOnShopping = $filter('filter')($localStorage.shopping, function(d){ return d.id==$scope.productLeft[index].id; })[0];
        } else if(side == 'right'){
          var productOnShopping = $filter('filter')($localStorage.shopping, function(d){ return d.id==$scope.productRight[index].id; })[0];
        }

        if(!productOnShopping){
          return false;
        } else {
          return true;
        }
      } else {
        return false;
      }
    };

    $scope.changeSearch = function(){
      var products = $filter('filter')($localStorage.products, function(d){
        var name = d.name,
        nameSearch = $scope.searchProducts.name,
        re = new RegExp(nameSearch, 'i'),
        res = name.match(re);

        if(!$scope.searchProducts.categoryId){
          if(res){
            return true;
          } else {
            return false;
          }
        } else {
          if($scope.searchProducts.categoryId == d.categoryId){
            if(res){
              return true;
            } else {
              return false;
            }
          } else {
            return false;
          }
        }


      });
      orderProducts(products);
    };

    /*
    * Description:
    *   Calculates the totalPrice to the item selected. This function is run by closeEditProduct, saveChange, addToShopping and onShopping.
    *
    * Parameters:
    *   typeUnit:   String. Indicates the type of unit selected
    *   quantity:   Int. The quantity of products was selected.
    */
    function getTotalPrice(typeUnit, quantity){
      var unitSelected = $filter('filter')($scope.productFound.units, function(d){ return d.unit==typeUnit; })[0];
      return quantity*unitSelected.price;
    };

    $rootScope.$on("CallParentMethod", function(){
      checkSearch();
    });

    /*
    * Description:
    *   Check the existence of categoryId in localStorage.searchProducts and apply to the search filter.
    */
    function checkSearch(){
      if(!$localStorage.searchProducts.categoryId){
        $scope.searchProducts = $localStorage.searchProducts = {};
        var products = $localStorage.products;
      } else {
        $scope.searchProducts = $localStorage.searchProducts;
        var products=$filter('filter')($localStorage.products, function(d){return d.categoryId==$scope.searchProducts.categoryId; });
      }
      orderProducts(products);
    };


    function orderProducts(data){
      /*if(!$localStorage.shopping){
        $localStorage.shopping = [];
      }*/
      $scope.productLeft = [];
      $scope.productRight = [];

      $scope.products = data;
      if($scope.products.length%2 != 0){
        for (var i = 0; i < $scope.products.length/2+.5; i++) {
          $scope.productLeft.push($scope.products[i]);
        }
        for (var i = $scope.products.length/2+.5; i < $scope.products.length; i++) {
          $scope.productRight.push($scope.products[i]);
        }
      } else {
        for (var i = 0; i < $scope.products.length/2; i++) {
          $scope.productLeft.push($scope.products[i]);
        }
        for (var i = $scope.products.length/2; i < $scope.products.length; i++) {
          $scope.productRight.push($scope.products[i]);
        }
      }
      $ionicScrollDelegate.scrollTop(true);
    };

    /*
    * Description:
    *   Initialize basic variables and scopes from the view.
    */
    function variables(){
      /*if(!$localStorage.shopping){
        $localStorage.shopping = [];
      }*/
      if(!$localStorage.searchProducts){
        $localStorage.searchProducts = {'name':"", 'categoryId':""};
      }
      $ionicLoading.show();

      Producto.find({},
        function success(data){
          $localStorage.products = data;
          orderProducts(data);
          $ionicLoading.hide();
        },
        function error(data){
          console.log('error');
          console.log(data);
          $ionicLoading.hide();
          alert("Error. Por favor intente de nuevo.");
        }
      );

      $ionicModal.fromTemplateUrl('main/app/dashboard/products/productModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.productModal = modal;
      });

      $ionicModal.fromTemplateUrl('main/app/shopping/editShoppingModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.editShoppingModal = modal;
      });
    };

    function init(){
      variables();
    };

    init();
  }
}();
