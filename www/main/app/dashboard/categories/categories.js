!function() {
  mainModule.config(function($stateProvider){
    $stateProvider
    .state('app.dashboard.categories', {
      url:"/categories",
      views: {
        dashboard: {
          templateUrl: 'main/app/dashboard/categories/categories.html',
          controller: controller
        }
      },
      resolve: {}
    })
  });
  function controller($rootScope, $scope, $state, $ionicLoading, $filter, $localStorage, Category){

    $scope.selectCategorie = function(index, side){
      if(index >= 0){
        if(side == 'left'){
          var categoryFound=$filter('filter')($scope.categoryLeft, function(d){return d.id==$scope.categoryLeft[index].id; })[0];
        } else if (side == 'right') {
          var categoryFound=$filter('filter')($scope.categoryRight, function(d){ return d.id==$scope.categoryRight[index].id; })[0];
        }
        $localStorage.searchProducts = {'categoryId': categoryFound.id};
      } else {
        $localStorage.searchProducts = {};
      }
      console.log($localStorage.searchProducts);
      $state.go('app.dashboard.products');
      $rootScope.$emit("CallParentMethod",{});
    };

    function variables(){
      $ionicLoading.show();

      $scope.categoryLeft = [];
      $scope.categoryRight = [];

      Category.find({},
        function success(data){
          $scope.categories = data;
          if($scope.categories.length%2 != 0){
            for (var i = 0; i < $scope.categories.length/2+.5; i++) {
              $scope.categoryLeft.push($scope.categories[i]);
            }
            for (var i = $scope.categories.length/2+.5; i < $scope.categories.length; i++) {
              $scope.categoryRight.push($scope.categories[i]);
            }
          } else {
            for (var i = 0; i < $scope.categories.length/2; i++) {
              $scope.categoryLeft.push($scope.categories[i]);
            }
            for (var i = $scope.categories.length/2; i < $scope.categories.length; i++) {
              $scope.categoryRight.push($scope.categories[i]);
            }
          }
          console.log($scope.categories);
          $ionicLoading.hide();
        },
        function error(data){
          console.log('error');
          console.log(data);
          $ionicLoading.hide();
          alert("Error. Por favor intente de nuevo.");
        }
      );

      /*$ionicModal.fromTemplateUrl('main/app/dashboard/products/productModal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
    $scope.productModal = modal;
  });

  $ionicModal.fromTemplateUrl('main/app/shopping/editShoppingModal.html', {
  scope: $scope,
  animation: 'slide-in-up'
}).then(function(modal) {
$scope.editShoppingModal = modal;
});*/
};

function init(){
  variables();
};

init();
}
}();
