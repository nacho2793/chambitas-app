! function() {
  mainModule.config(function($stateProvider) {
    $stateProvider
      .state('app.orders', {
        url: "/orders",
        cache: false,
        views: {
          app: {
            templateUrl: 'main/app/orders/orders.html',
            controller: controller
          }
        },
        resolve: {}
      })
  });

  function controller($scope, $state, $ionicModal, $filter, $ionicLoading, $ionicScrollDelegate, $ionicPopup, Sale, Client) {

    /*
     * Description:
     *   Close Modal infoOrderModal.
     */
    $scope.closeInfoOrderModal = function() {
      $ionicScrollDelegate.scrollTop(true);
      $scope.editEnabled = false;
      $scope.infoOrderModal.hide();
    };

    /*
     * Description:
     *   Applies the filter to ngRepeat of orders and shows only the status = "En espera".
     */
    $scope.orderHold = function() {
      $scope.searchSales.status = "En espera";
      $ionicScrollDelegate.scrollTop(true);
    };

    /*
     * Description:
     *   Applies the filter to ngRepeat of orders and shows only the status = "Enviado".
     */
    $scope.orderSent = function() {
      $scope.searchSales.status = "Enviado";
      $ionicScrollDelegate.scrollTop(true);
    };

    /*
     * Description:
     *   Applies the filter to ngRepeat of orders and shows only the status = "Entregado".
     */
    $scope.orderDelivered = function() {
      $scope.searchSales.status = "Entregado";
      $ionicScrollDelegate.scrollTop(true);
    };

    /*
     * Description:
     *   Change the status to "Cancelado" on Sale and update the new sales.
     */
    $scope.cancelOrder = function() {
      var alertPopup = $ionicPopup.confirm({
        title: 'Alerta',
        template: '¿Estas seguro de que deseas eliminar este pedido?'
      });

      alertPopup.then(function(res) {
        if (res) {
          $ionicLoading.show();
          var idSale = $scope.saleFound.id;
          Sale.prototype$patchAttributes({
              'id': idSale
            }, {
              'status': 'Cancelado'
            },
            function success(data) {
              console.log(data);
              findSales();
              $ionicLoading.hide();
              $scope.closeInfoOrderModal();
            },
            function error(data) {
              console.log(data);
              $ionicLoading.hide();
            });
        }
      });
    };

    /*
     * Description:
     *   Change the status to "Entregado" on Sale and update the new sales.
     */
    $scope.orderReceived = function() {
      $ionicLoading.show();
      var idSale = $scope.saleFound.id;
      var deliveredDate = new Date();
      Sale.prototype$patchAttributes({
          'id': idSale
        }, {
          'status': 'Entregado',
          'deliveredDate': deliveredDate
        },
        function success(data) {
          console.log(data);
          findSales();
          $ionicLoading.hide();
          $scope.closeInfoOrderModal();
        },
        function error(data) {
          console.log(data);
          $ionicLoading.hide();
        });
    };

    /*
     * Description:
     *   Search the item selected on the orders and show infoOrderModal.
     *
     * Parameters:
     *   id:    Int. Id from the item selected on the list of orders.
     */
    $scope.selectOrder = function(id) {
      console.log(id);
      $scope.saleFoundOrig = $filter('filter')($scope.sales, function(d) {
        return d.id == id;
      })[0];
      $scope.saleFound = Object.assign({}, $scope.saleFoundOrig);
      $scope.infoOrderModal.show();
    };

    /*
     * Description:
     *   Enable Button "Modificar" and hide "Modificar pedido"
     */
    $scope.cancelChanges = function() {
      $scope.editEnabled = false;
    };

    /*
     * Description:
     *   Send new data to Backend on Sale selected.
     */
    $scope.sendChanges = function() {
      var minDateSale = new Date();
      minDateSale.setHours(48);
      minDateSale.setMinutes(0);
      var desiredDate = moment([
        moment($scope.saleFound.date_form).get('year'),
        moment($scope.saleFound.date_form).get('month'),
        moment($scope.saleFound.date_form).get('date'),
        moment($scope.saleFound.time_form).get('hour'),
        moment($scope.saleFound.time_form).get('minute')
      ]);

      if (desiredDate > minDateSale) {
        console.log(desiredDate);
        console.log(minDateSale);
        $ionicLoading.show();
        var idSale = $scope.saleFound.id;
        var newData = {
          'comments': $scope.saleFound.comments,
          'desiredDate': desiredDate
        };
        Sale.prototype$patchAttributes({
            'id': idSale
          }, newData,
          function succes(data) {
            /*
            En espera de Backend
            Notification.send({method: 'email', type: 'saleRequest', userId:'', extraInfo:{} });
            Notification.send({method: 'email', type: 'saleRequest', userId:'', extraInfo:{} });
            */
            console.log(data);
            findSales();
            $ionicLoading.hide();
            $scope.saleFound.desiredDate = data.desiredDate;
            $scope.editEnabled = false;
            $ionicScrollDelegate.scrollTop(true);
          },
          function error(data) {
            console.log(data);
            $ionicLoading.hide();
          });
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'Alerta',
          template: 'La Fecha de entrega debe ser minimo dentro de dos dias.'
        });

        alertPopup.then();
      }
    };

    /*
     * Description:
     *   Disable Button "Modificar" and show "Modificar pedido"
     */
    $scope.enableEdit = function() {
      initiazTime();
      $scope.editEnabled = true;
      $ionicScrollDelegate.scrollTop(true);
    };

    /*
     * Description:
     *   Initialize the time_form to handle accurate minute intervals.
     */
    function initiazTime() {
      if (!$scope.saleFound.time_form) {
        $scope.saleFound.time_form = new Date();
        switch (true) {
          case moment().get('minute') > 0 && moment().get('minute') <= 7:
            $scope.saleFound.time_form.setMinutes(0);
            break;
          case moment().get('minute') > 7 && moment().get('minute') <= 22:
            $scope.saleFound.time_form.setMinutes(15);
            break;
          case moment().get('minute') > 22 && moment().get('minute') <= 37:
            $scope.saleFound.time_form.setMinutes(30);
            break;
          case moment().get('minute') > 37 && moment().get('minute') <= 52:
            $scope.saleFound.time_form.setMinutes(45);
            break;
          case moment().get('minute') > 52 && moment().get('minute') <= 59:
            $scope.saleFound.time_form.setMinutes(0);
            break;
          default:
            $scope.saleFound.time_form.setMinutes(0);
        }
      }

      if (!$scope.saleFound.date_form) {
        $scope.saleFound.date_form = new Date();
        $scope.saleFound.date_form.setHours(48)
      }
    };

    /*
     * Description:
     *   Download from the Backend all Sales of the current user
     */
    function findSales() {
      var idToSearch = Client.getCurrentId();
      console.log(idToSearch);

      Sale.find({
          filter: {
            where: {
              'clientId': idToSearch
            }
          }
        },
        function success(data) {
          console.log('success');
          console.log(data);
          $scope.sales = data;
          $ionicLoading.hide();
        },
        function error(data) {
          console.log('error');
          console.log(data);
          alert("Error. Por favor intente de nuevo.");
          $ionicLoading.hide();
        }
      );
    };

    /*
     * Description:
     *   Initialize basic variables and scopes from the view.
     */
    function variables() {
      $ionicLoading.show();
      $scope.searchSales = {
        'status': 'En espera'
      };
      $scope.editEnabled = false;

      findSales();

      $ionicModal.fromTemplateUrl('main/app/orders/infoOrderModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.infoOrderModal = modal;
      });
    };

    function init() {
      variables();
    };

    init();
  }
}();
