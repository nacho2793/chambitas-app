!function() {
  mainModule.config(function($stateProvider){
    $stateProvider
      .state('start', {
        url:"/start",
        abstract: true,
        views: {
          main: {
            template: '<ion-nav-view name="start"></ion-nav-view>',
            controller: controller
          }
        },
        resolve: {}
      })
  });
  
  function controller($scope){}
}();
