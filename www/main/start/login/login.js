! function() {
  mainModule.config(function($stateProvider) {
    $stateProvider
      .state('start.login', {
        url: "/login",
        cache: false,
        views: {
          start: {
            templateUrl: 'main/start/login/login.html',
            controller: controller
          }
        },
        resolve: {}
      })
  });

  function controller($scope, $state, AppUser, $ionicLoading, $ionicHistory) {
    $scope.gotoRecPass = function() {
      $state.go('start.recPass');
    };

    function showSpinner() {

    }

    //define variables for this controller
    function variables() {
      $scope.credentials = {
        email: "",
        password: ""
      };
      $scope.confirmPassword = '';
      $scope.loader = {
        active: true
      };
    }


    // login function
    $scope.login = function() {
      $scope.loader.active = false;

      

    };

    //chack user loged
    function isAuthenticated() {
      
    }


    function init() {
      variables();
      isAuthenticated();

    }

    init();
  }
}();
